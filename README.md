# Webinaire de la Faculté des sciences et de génie

## Titre

Maman, j'ai (encore) perdu les résultats de mon rapport!

## Description

C'est arrivé à tout le monde: la dernière version d'un rapport perdue
parmi les multiples autres versions; le texte du rapport qui ne
correspond pas aux résultats; le code informatique qui fonctionnait
hier et qui ne fonctionne plus aujourd'hui... Pourtant, avec les bons
outils et en suivant quelques principes de la «recherche
reproductible», il est possible d'éviter ce genre de situations
fâcheuses.

## Objectif principal

Lors de cette présentation, Vincent Goulet passera en revue les
principes de base de la «recherche reproductible» et quelques outils
qui vont permettront de gagner en productivité, vous et vos équipes de
travail. 

La présentation s'adresse à toute personne qui effectue du
travail scientifique à partir de données et qui doit produire des
analyses ou des rapports. Les exemples utiliseront R, Python et Git,
mais les principes sont applicables à d'autres outils.

## À propos du conférencier

Vincent Goulet est professeur à l'École d'actuariat de l'Université
Laval. Utilisateur et développeur R depuis près de 20 ans, il a
profondément modifié ses méthodes de travail en adoptant graduellement
les outils de recherche reproductible.
